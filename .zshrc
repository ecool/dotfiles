if [[ -e $HOME/.zsh ]]; then
    for file in `ls --hide='*~' $HOME/.zsh`;
        do source $HOME/.zsh/$file;
    done;
fi
# Add some to run on login

if [[ -x $(whence -p fortune) ]]; then
  echo
  fortune
fi
cd $HOME;
# Personal Aliases, Functions etc.
if [[ -f $HOME/.zsh_aliases ]]; then
    source $HOME/.zsh_aliases
fi
if [[ -f $HOME/.ssh-login ]]; then
#    source $HOME/.ssh-login
fi
#how_many_cpu;
