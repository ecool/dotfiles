#! /bin/bash

# TODO: make all system installs force yes

# TODO: update system to latest release
sudo pacman -Syu --noconfirm

# Aura requirements
sudo pacman -S wget curl gmp pacman-color pcre --noconfirm

# get Aura from AUR
echo "wget Aura"
wget https://aur.archlinux.org/packages/au/aura/aura.tar.gz
echo "Untar Aura"
tar zxf aura.tar.gz
cd aura
echo "Make Aura"
makepkg -s
sudo pacman -U aura.tar.xz --noconfirm


# get git clone of dotfiles repo
sudo aura -S git
cd ~
mkdir temp
cd temp
git clone https://ecool@bitbucket.org/ecool/dotfiles.git




# X
sudo aura -S xorg-server xorg-apps --noconfirm
# TODO: move xorg.conf to /etc/X11/xorg.conf
# TODO: move .xinitrc to ~/.xinitrc


# Fluxbox
sudo aura -S fluxbox --noconfirm
sudo aura -S fluxbox-styles --noconfirm
# style -> laser_allblack
# TODO: find where style is set in configs and change it

sudo aura -S fbpanel --noconfirm
sudo aura -S idesk --noconfirm
# TODO: move fbpanel configuration files to ~/.config/fbpanel/


# GTK themes
# 2.0
# Mire v2 Blue
# location: ~/.themes/Mire v2 Blue
# TODO: edit files that keep GTK theme information



# Sound

sudo aura -S paprefs --noconfirm
sudo aura -S pavucontrol --noconfirm
sudo aura -S lib32-alsa-plugins lib32-pulseaudio --noconfirm
sudo aura -S openal --noconfirm
sudo aura -S lib32-openal --noconfirm


# Wine

sudo aura -A wine-git --noconfirm
sudo aura -S winetricks --noconfirm
sudo aura -A wine-mono --noconfirm
sudo aura -S wine_gecko --noconfirm


# Game requirements
# SC 2
sudo aura -S lib32-libjpeg-turbo lib32-libpng lib32-libldap --noconfirm
sudo aura -S nvidia nvidia-utils lib32-nvidia-utils --noconfirm
# SC 2 editor
winetricks vcrun2005 vcrun2008 --noconfirm # TODO: double check winetricks options
sudo aura -S lib32-mpg123 lib32-jack --noconfirm


# Python
sudo aura -S python-distribute --noconfirm



sudo aura -S conky-all --noconfirm
sudo aura -A xsltproc --noconfirm
sudo fc-cache -fv
sudo aura -S libxslt --noconfirm
sudo aura -S lua --noconfirm

sudo aura -A archey --noconfirm
sudo aura -S scrot --noconfirm
sudo aura -A cairo-compmgr-git --noconfirm
sudo aura -S curl --noconfirm

#Chrome and Chromium
sudo aura -S chromium --noconfirm

# setup/install UDev rules for USB and TrueCrypt stuff
sudo aura -S truecrypt --noconfirm




# Setup/Install PAM_USB and PAM_FACERECOGNITION
# TODO: figure it out


